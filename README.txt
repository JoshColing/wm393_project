##############     U2161341 PROJECT      ##############


REPORT IS IN Write_Up directory 


TO RUN THE PROJECT PLEASE OPEN A TERMINAL TO WM393_PROJECT.
1. In my case it is '/Users/joshcoling/Documents/WM393_Git/wm393_projectv2/WM393_Project'

2. RUN THIS COMMAND: 'python manage.py runserver' or 'python3 manage.py runserver'
3. Go to localhost and you should have the website. 


PROBLEMS THAT MAY OCCUR
Pip modules not installed. My project has several modules these include:
   - django
   - plotly
   - pandas
   - faker

   (Just pip install them) using the pip command: 'pip install {modulename}' or 'pip3 install {modulename}'

logins accounts: 
    Student: 
    •	Username: u2161341
    •	Password: WM393_Module_PW
    Teacher: 
    •	Username: Park_Young
    •	Password: WM393_Module_PW
    Superuser:
    •	Username: joshcoling
    •	Password: Admin

If you have any other problems they are due to your machine and 
already running code.
This has been tested on multiple machines



