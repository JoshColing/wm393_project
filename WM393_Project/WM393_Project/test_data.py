from faker import Faker
from WM393_App.models import Student_Info

fake = Faker()

# Create 50 Student_Info objects with random names and surnames
students = []
for i in range(50):
    student = Student_Info(
        student_id=fake.random_int(min=100000, max=999999),
        name=fake.first_name(),
        surname=fake.last_name(),
        email=fake.email()
    )
    students.append(student)

# Save the Student_Info objects to the database
Student_Info.objects.bulk_create(students)
