from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
import plotly.graph_objs as go
import plotly.offline as opy
import pandas as pd
from .models import Modules, Module_Satisfaction, Attendance, Grades, Diversity, Course_Registration, Course_Retention
from collections import defaultdict
from django.utils.datastructures import MultiValueDictKeyError




def landing_page(request):
    return render(request, 'landing_page.html')

def about_us(request):
    return render(request, 'about-us.html')


def login_page(request):
    invalid_login = False
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                if user.groups.filter(name='Student').exists():
                    return redirect('module-satisfaction')
                elif user.groups.filter(name='Teachers').exists():
                    return redirect('teacher-panel')
            else:
                invalid_login = True
        except MultiValueDictKeyError:
            pass

    context = {
        'invalid_login': invalid_login
    }
    return render(request, 'login.html', context)



def module_satisfaction(request):
    return render(request, 'module-satisfaction.html')


def teacher_panel(request):
    
    if request.method == 'POST' and 'attendance-btn' in request.POST:
        attendance = Attendance.objects.values('module_id', 'attendance')
        attendance_df = pd.DataFrame.from_records(attendance)

        # Retrieve module names from the database
        module_names = {m.id: m.name for m in Modules.objects.all()}

        # Group attendance statuses into even groups for each module
        attendance_by_module = defaultdict(pd.Series)
        for module_id, module_data in attendance_df.groupby('module_id'):
            attendance_groups = pd.cut(module_data['attendance'], bins=[-1, 50, 100, 150], labels=['Presant', 'Partially Present', 'Absent'])
            attendance_counts = attendance_groups.value_counts()
            
            attendance_by_module[module_id] = attendance_counts

        # Format data for chart generation
        chart_data = []

        for module_id, attendance_counts in attendance_by_module.items():
            module_name = module_names[module_id]
            chart_div = generate_pie_chart(attendance_counts, module_name)
            chart_data.append({
                'name': module_name,
                'chart_div': chart_div,
            })
            if len(chart_data) == 4:
                break
        # Render the template with context containing chart data
        context = {'chart_data': chart_data}
        return render(request, 'teacher-panel.html', context)


    elif request.method == 'POST' and 'module-satisfaction' in request.POST:

        module_satisfaction = Module_Satisfaction.objects.values('module_id', 'satisfaction')
        module_satisfaction_df = pd.DataFrame.from_records(module_satisfaction)

        # Retrieve module names from the database
        module_names = {m.id: m.name for m in Modules.objects.all()}

        # Group satisfaction levels into even groups for each module
        satisfaction_by_module = defaultdict(pd.Series)
        for module_id, module_data in module_satisfaction_df.groupby('module_id'):
            satisfaction_groups = pd.cut(module_data['satisfaction'], bins=[-1, 19, 39, 59, 79, 100], labels=['0-19', '20-39', '40-59', '60-79', '80-100'])
            satisfaction_counts = satisfaction_groups.value_counts()
            
            satisfaction_by_module[module_id] = satisfaction_counts

        # Format data for chart generation
        chart_data = []

        for module_id, satisfaction_counts in satisfaction_by_module.items():
            module_name = module_names[module_id]
            chart_div = generate_pie_chart(satisfaction_counts, module_name)
            chart_data.append({
                'name': module_name,
                'chart_div': chart_div,
            })
            if len(chart_data) == 4:
                break
        # Render the template with context containing chart data
        context = {'chart_data': chart_data}
        return render(request, 'teacher-panel.html', context)
    
    elif request.method == 'POST' and 'grades-btn' in request.POST:
        grades = Grades.objects.values('module_id', 'grade')
        grades_df = pd.DataFrame.from_records(grades)

        # Retrieve module names from the database
        module_names = {m.id: m.name for m in Modules.objects.all()}

        # Map grades to numeric values
        grade_mapping = {'F': 0, '3': 1, '2:2': 2, '2:1': 3, '1': 4}
        grades_df['grade_numeric'] = grades_df['grade'].map(grade_mapping)

        # Group grades into even groups for each module
        grades_by_module = defaultdict(pd.Series)
        for module_id, module_data in grades_df.groupby('module_id'):
            grade_groups = pd.cut(module_data['grade_numeric'], bins=[-1, 0.5, 1.5, 2.5, 3.5, 4.5], labels=['F', '3', '2:2', '2:1', '1'])
            grade_counts = grade_groups.value_counts()
            
            grades_by_module[module_id] = grade_counts

        # Format data for chart generation
        chart_data = []

        for module_id, grade_counts in grades_by_module.items():
            module_name = module_names[module_id]
            chart_div = generate_pie_chart(grade_counts, module_name)
            chart_data.append({
                'name': module_name,
                'chart_div': chart_div,
            })
            if len(chart_data) == 4:
                break
        # Render the template with context containing chart data
        context = {'chart_data': chart_data}
        return render(request, 'teacher-panel.html', context)
    
    elif request.method == 'POST' and 'diversity-btn' in request.POST:
        diversity = Diversity.objects.values('module_id', 'race')
        diversity_df = pd.DataFrame.from_records(diversity)

        # Retrieve module names from the database
        module_names = {m.id: m.name for m in Modules.objects.all()}

        # Group students by race for each module
        diversity_by_module = defaultdict(pd.Series)
        for module_id, module_data in diversity_df.groupby('module_id'):
            race_counts = module_data['race'].value_counts()
            
            # Pad race counts with zero values for missing races
            race_counts = race_counts.reindex(['Black', 'White', 'Asian', 'Hispanic'], fill_value=0)
            
            diversity_by_module[module_id] = race_counts

        # Format data for chart generation
        chart_data = []

        for module_id, race_counts in diversity_by_module.items():
            module_name = module_names[module_id]
            chart_div = generate_pie_chart(race_counts, module_name)
            chart_data.append({
                'name': module_name,
                'chart_div': chart_div,
            })
            if len(chart_data) == 4:
                break
        # Render the template with context containing chart data
        context = {'chart_data': chart_data}
        return render(request, 'teacher-panel.html', context)
    
    elif request.method == 'POST' and 'course-registration-btn' in request.POST:
        # Retrieve enrollment data from the database
        course_registrations = Course_Registration.objects.values('module_id', 'enrolment', 'temporary_withdrawal', 'tuition_paid', 'its_account', 'email_registration')
        course_registrations_df = pd.DataFrame.from_records(course_registrations)

        # Retrieve module names from the database
        module_names = {m.id: m.name for m in Modules.objects.all()}

        # Format data for chart generation
        chart_data = []

        for module_id, module_data in course_registrations_df.groupby('module_id'):
            module_name = module_names[module_id]
            
            # Get data for each label
            enrolled = module_data['enrolment'].sum()
            not_enrolled = module_data['temporary_withdrawal'].sum()
            tuition_paid = module_data['tuition_paid'].sum()
            its_account = module_data['its_account'].sum()
            email_registration = module_data['email_registration'].sum()

            # Create x and y axis data for the bar chart
            x_axis = ['Enrolled', 'Temporary\nWithdrawal', 'Tuition\nPaid', 'ITS\nAccount', 'Email\nRegistration']
            y_axis = [enrolled, not_enrolled, tuition_paid, its_account, email_registration]

            # Generate bar chart
            chart_div = generate_bar_chart(x_axis, y_axis, module_name)

            chart_data.append({
                'name': module_name,
                'chart_div': chart_div,
            })

            if len(chart_data) == 4:
                break

        # Render the template with context containing chart data
        context = {'chart_data': chart_data}
        return render(request, 'teacher-panel.html', context)


    elif request.method == 'POST' and 'course-retention-btn' in request.POST:
        # Retrieve course retention data from the database
        course_retention = Course_Retention.objects.values('module_id', 'year_1', 'year_2', 'year_3', 'year_4')
        course_retention_df = pd.DataFrame.from_records(course_retention)

        # Retrieve module names from the database
        module_names = {m.id: m.name for m in Modules.objects.all()}

        # Group course retention levels by year for each module
        retention_by_module = defaultdict(pd.Series)
        for module_id, module_data in course_retention_df.groupby('module_id'):
            retention_by_year = module_data.iloc[:, 1:].sum().astype(int)
            retention_by_module[module_id] = retention_by_year

        # Format data for chart generation
        chart_data = []

        for module_id, retention_counts in retention_by_module.items():
            module_name = module_names[module_id]
            chart_div = generate_line_chart(retention_counts, module_name)
            chart_data.append({
                'name': module_name,
                'chart_div': chart_div,
            })
            if len(chart_data) == 4:
                break

        # Render the template with context containing chart data
        context = {'chart_data': chart_data}
        return render(request, 'teacher-panel.html', context)
    

    return render(request, 'teacher-panel.html')

def generate_pie_chart(data, title):
    # Split title by whitespace
    title_parts = title.split()

    # Concatenate title parts with '\n' as separator
    formatted_title = '\n'.join(title_parts)

    trace = go.Pie(labels=data.index, values=data.values)
    layout = go.Layout(title=formatted_title)
    fig = go.Figure(data=[trace], layout=layout)
    div = opy.plot(fig, auto_open=False, output_type='div')
    return div

def generate_bar_chart(x_data, y_data, title):
    trace = go.Bar(x=x_data, y=y_data)
    layout = go.Layout(title=title, xaxis={'title': 'Categories'}, yaxis={'title': 'Number of Students'})
    fig = go.Figure(data=[trace], layout=layout)
    div = opy.plot(fig, auto_open=False, output_type='div')
    return div

def generate_line_chart(data, title):
    trace = go.Scatter(x=data.index, y=data.values, mode='lines+markers')
    layout = go.Layout(title=title, xaxis=dict(title='Year'), yaxis=dict(title='Number of Students'))
    fig = go.Figure(data=[trace], layout=layout)
    div = opy.plot(fig, auto_open=False, output_type='div')
    return div



