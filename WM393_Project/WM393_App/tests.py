from django.test import TestCase
from .models import Student_Info, Modules, Module_Satisfaction, Attendance, Diversity, Grades, Course_Registration, Course_Retention

class StudentInfoModelTestCase(TestCase):
    def setUp(self):
        self.student = Student_Info.objects.create(student_id='1234567890')

    def test_student_info_creation(self):
        student = Student_Info.objects.get(student_id='1234567890')
        self.assertEqual(student.student_id, '1234567890')


class ModuleSatisfactionModelTestCase(TestCase):
    def setUp(self):
        self.student = Student_Info.objects.create(student_id='1234567890')
        self.module = Modules.objects.create(module_id='MATH101', name='Calculus')
        self.module_satisfaction = Module_Satisfaction.objects.create(student=self.student, module=self.module, satisfaction=5, comment='Great class!')

    def test_module_satisfaction_creation(self):
        module_satisfaction = Module_Satisfaction.objects.get(student=self.student, module=self.module)
        self.assertEqual(module_satisfaction.satisfaction, 5)
        self.assertEqual(module_satisfaction.comment, 'Great class!')

class AttendanceModelTestCase(TestCase):
    def setUp(self):
        self.student = Student_Info.objects.create(student_id='1234567890')
        self.module = Modules.objects.create(module_id='MATH101', name='Calculus')
        self.attendance = Attendance.objects.create(student=self.student, module=self.module, attendance=90)

    def test_attendance_creation(self):
        attendance = Attendance.objects.get(student=self.student, module=self.module)
        self.assertEqual(attendance.attendance, 90)

class DiversityModelTestCase(TestCase):
    def setUp(self):
        self.student = Student_Info.objects.create(student_id='1234567890')
        self.module = Modules.objects.create(module_id='MATH101', name='Calculus')
        self.diversity = Diversity.objects.create(student=self.student, module=self.module, race='Asian')

    def test_diversity_creation(self):
        diversity = Diversity.objects.get(student=self.student, module=self.module)
        self.assertEqual(diversity.race, 'Asian')

class GradesModelTestCase(TestCase):
    def setUp(self):
        self.student = Student_Info.objects.create(student_id='1234567890')
        self.module = Modules.objects.create(module_id='MATH101', name='Calculus')
        self.grade = Grades.objects.create(student=self.student, module=self.module, grade='A')

    def test_grade_creation(self):
        grade = Grades.objects.get(student=self.student, module=self.module)
        self.assertEqual(grade.grade, 'A')


class CourseRegistrationTest(TestCase):
    def setUp(self):
        self.student = Student_Info.objects.create(student_id='S001')
        self.module = Modules.objects.create(module_id='M001', name='Intro to Django')
        self.course_registration = Course_Registration.objects.create(
            student=self.student,
            module=self.module,
            enrolment=True,
            temporary_withdrawal=False,
            tuition_paid=True,
            its_account='A001',
            email_registration=True
        )

    def test_course_registration_created(self):
        self.assertEqual(self.course_registration.__str__(), f"{self.student.student_id} - {self.module.name}")
        self.assertEqual(self.course_registration.enrolment, True)
        self.assertEqual(self.course_registration.temporary_withdrawal, False)
        self.assertEqual(self.course_registration.tuition_paid, True)
        self.assertEqual(self.course_registration.its_account, 'A001')
        self.assertEqual(self.course_registration.email_registration, True)



class CourseRetentionTest(TestCase):
    def setUp(self):
        self.student = Student_Info.objects.create(student_id='S001')
        self.module = Modules.objects.create(module_id='M001', name='Intro to Django')
        self.course_retention = Course_Retention.objects.create(
            student=self.student,
            module=self.module,
            year_1=True,
            year_2=False,
            year_3=True,
            year_4=False
        )

    def test_course_retention_created(self):
        self.assertEqual(self.course_retention.__str__(), f"{self.student.student_id} - {self.module.name}")
        self.assertEqual(self.course_retention.year_1, True)
        self.assertEqual(self.course_retention.year_2, False)
        self.assertEqual(self.course_retention.year_3, True)
        self.assertEqual(self.course_retention.year_4, False)


