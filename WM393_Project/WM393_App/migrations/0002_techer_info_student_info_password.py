# Generated by Django 4.1.7 on 2023-03-05 10:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('WM393_App', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='techer_Info',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('teacher_id', models.CharField(max_length=10)),
                ('name', models.CharField(max_length=50)),
                ('surname', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('password', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='student_info',
            name='password',
            field=models.CharField(default='WMGSIS', max_length=50),
        ),
    ]
