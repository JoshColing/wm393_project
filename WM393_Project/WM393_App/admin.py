from django.contrib import admin
from .models import Student_Info, Modules, Module_Satisfaction, Attendance, Diversity, Grades, Course_Registration, Course_Retention

admin.site.register(Student_Info)
admin.site.register(Modules)
admin.site.register(Module_Satisfaction)
admin.site.register(Attendance)
admin.site.register(Diversity)
admin.site.register(Grades)
admin.site.register(Course_Registration)
admin.site.register(Course_Retention)
