from django.db import models

class Student_Info(models.Model):
    student_id = models.CharField(max_length=10)
    username = models.CharField(max_length=50, default='Example_firstname')
    surname = models.CharField(max_length=50, default='Example_surname')
    email = models.EmailField(default='example@example.com')



class Modules(models.Model):
    module_id = models.CharField(max_length=10)
    name = models.CharField(max_length=50)

class Module_Satisfaction(models.Model):
    student = models.ForeignKey(Student_Info, on_delete=models.CASCADE)
    module = models.ForeignKey(Modules, on_delete=models.CASCADE)
    satisfaction = models.IntegerField()
    comment = models.TextField()

class Attendance(models.Model):
    student = models.ForeignKey(Student_Info, on_delete=models.CASCADE)
    module = models.ForeignKey(Modules, on_delete=models.CASCADE)
    attendance = models.IntegerField()

class Diversity(models.Model):
    student = models.ForeignKey(Student_Info, on_delete=models.CASCADE)
    module = models.ForeignKey(Modules, on_delete=models.CASCADE)
    race = models.CharField(max_length=50)

class Grades(models.Model):
    student = models.ForeignKey(Student_Info, on_delete=models.CASCADE)
    module = models.ForeignKey(Modules, on_delete=models.CASCADE)
    grade = models.CharField(max_length=10)

class Course_Registration(models.Model):
    student = models.ForeignKey(Student_Info, on_delete=models.CASCADE)
    module = models.ForeignKey(Modules, on_delete=models.CASCADE)
    enrolment = models.BooleanField(default=False)
    temporary_withdrawal = models.BooleanField(default=False)
    tuition_paid = models.BooleanField(default=False)
    its_account = models.CharField(max_length=20)
    email_registration = models.BooleanField(default=False)
    vs = models.CharField(max_length=20)

class Course_Retention(models.Model):
    student = models.ForeignKey(Student_Info, on_delete=models.CASCADE)
    module = models.ForeignKey(Modules, on_delete=models.CASCADE)
    year_1 = models.BooleanField(default=False)
    year_2 = models.BooleanField(default=False)
    year_3 = models.BooleanField(default=False)
    year_4 = models.BooleanField(default=False)

